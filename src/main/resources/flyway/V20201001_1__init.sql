DROP TABLE IF EXISTS person CASCADE;
CREATE TABLE person (
    id SERIAL PRIMARY KEY,
    firstname VARCHAR(255),
    lastname VARCHAR(255),
    city VARCHAR(255),
    phone int,
    telegram int
);
Insert Into person(firstname, lastname, city, phone, telegram) VALUES
('Arman','asdfas','kar',123456,4565315),
('Bekzat','asdfas','ala',123456,4565315),
('Almaz','asdfas','ast',123456,4565315);
