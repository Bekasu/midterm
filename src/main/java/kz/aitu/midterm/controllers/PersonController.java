package kz.aitu.midterm.controllers;

import kz.aitu.midterm.models.Person;
import kz.aitu.midterm.services.PersonService;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
public class PersonController {
    private final PersonService personService;

    public PersonController(PersonService personService){
        this.personService = personService;
    }

    @RequestMapping("/")
    public String getIndex() {
        return "index";
    }

    @GetMapping("/api/v2/users/all")
    public ResponseEntity<?> getAll(){
        return personService.getAll();
    }

    @PostMapping("/api/v2/users/")
    public ResponseEntity<?> createPerson(@RequestBody Person person){
        return ResponseEntity.ok(personService.createPerson(person));
    }

    @PutMapping("/api/v2/users/")
    public ResponseEntity<?> updatePerson(@RequestBody Person person){
        return ResponseEntity.ok(personService.updatePerson(person));
    }

    @DeleteMapping("/api/v2/users/{id}")
    public void deleteByID(Long id){
        personService.deleteByID(id);
    }

}
