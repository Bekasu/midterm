package kz.aitu.midterm.services;

import kz.aitu.midterm.models.Person;
import kz.aitu.midterm.repositories.PersonRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class PersonService {
    private final PersonRepository personRepository;

    public PersonService(PersonRepository personRepository){
        this.personRepository = personRepository;
    }

    public ResponseEntity<?> getAll(){
        return ResponseEntity.ok(personRepository.findAll());
    }

    public ResponseEntity<?> getById(Long id){
        return ResponseEntity.ok(personRepository.findById(id));
    }

    public void deleteByID(Long id){
        personRepository.deleteById(id);
    }

    public Person createPerson(Person person){
        return personRepository.save(person);
    }

    public Person updatePerson(Person person){
        return personRepository.save(person);
    }
}
