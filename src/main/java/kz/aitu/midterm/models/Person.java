package kz.aitu.midterm.models;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@Entity
@Table(name = "person")
public class Person {
    @Id
    private int Long;
    private String firstname;
    private String lastname;
    private String city;
    private int phone;
    private int telegram;
}
